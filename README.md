# Psyfire Blog

Welcome to the Psyfire Blog!  This blog will include a variety of topics including projects I'm currently working on, philosophy, cooking, art, programming, graphics, and more!

# Recent Articles

* [Maintainable Tests and Functional Programming](/code/fp/verify-prototype.md)
 
# Topics

* [Programming](/code)
 
# Prototype

This blog is currently in a highly experimental prototype phase, so it's possible there may be several changes as I experiment with different types of content, and ways to better organize articles.