# Maintainable Tests and Functional Programming
Perhaps the two most commonly neglected responsibilities in programming are **testing** and **documentation**.  Admittedly, both aren't very fun to create; they're typically tedious and verbose to create.  Test in particular often feel like an overhead cost, red-tape that adds little to functionality.  As a result, most software-tests are verbose, ugly, copy-paste monstrosities, and devs write as few as possible to avoid maintaining those unreadable balls of mud.

While working on a personal project, I happened to have some code that would benefit from testing.  I also wanted to verify approximately 15 different test-cases.  It didn't take long before I had test-code that was driving my programmer-OCD insane, due to it becomming somewhat of a verbose ball of mud.

## Cleaning Up Test Code

For simplicity purposes, we'll work with some over-simplified test code, and just 5 test-cases:

```scala
import org.scalatest.FunSuite
import org.scalactic.Tolerance._

class SomeClass (a: Float){
  def someFunction(b: Float, c: Float): Float = {
    a + b + c
  }
}

class VerifyTestExample extends FunSuite {
  private val TINY = 0.000001f

  test("Asserts, asserts everywhere") {
    assert(new SomeClass(0).someFunction(0,0)   === 0f +- TINY)
    assert(new SomeClass(0).someFunction(5,5)   === 10f +- TINY)
    assert(new SomeClass(10).someFunction(1,2)  === 13f +- TINY)
    assert(new SomeClass(10).someFunction(5,5)  === 20f +- TINY)
    assert(new SomeClass(-10).someFunction(5,5) === 0f +- TINY)
  }
}
```
This isn't too bad in our simplified example, but there is some annoying verbosity and duplication.  If we're willing to be a little clever, it's easy to eliminate some of that code-duplication.
```scala
  test("Extract a function, clever!") {
    def verifyFunction(a: Float, b: Float, c: Float, expected: Float): Unit = {
      assert( new SomeClass(a).someFunction(b,c) === expected +- TINY)
    }
    verifyFunction(0,0,0,   expected = 0f )
    verifyFunction(0,5,5,   expected = 10f)
    verifyFunction(10,1,2,  expected = 13f)
    verifyFunction(10,5,5,  expected = 20f)
    verifyFunction(-10,5,5, expected = 0f)
  }
```
We've cut some verbosity, but also lost some readability, and the `verifyFunction` boilerplate remains.  If you stopped there, you might be wondering "why waste our time?"  Trying, and perhaps failing several times, and iterating is how we find better solutions that actually do work very well!  Without wasting too much time, lets fastforward a few steps.
```scala
  test("Now we're playing with functions!") {
    Seq(
      //a | b |  c | expected
      (0f,  0f, 0f, 0f),
      (0f,  5f, 5f, 10f),
      (10f, 1f, 2f, 13f),
      (10f, 5f, 5f, 20f),
      (-10f,5f, 5f, 0f),
    ).foreach{case (a:Float, b:Float, c:Float, expected: Float) =>
      assert(new SomeClass(a).someFunction(b,c) === expected +- TINY)
    }
  }
```

Writing 20+ test-cases this way would be quite easy.  Each test-case is effectively a row on a table, and adding one more would be just another row.  The definition of each column (tuple param) is defined in the case-statement, and the code we're verifying is on the line beneath.  If you write code like shown above, it's good to put some extra effort into readability.  For production code, that's probably a decent place to stop as it requires no dependencies or additional code.

## The "Verify" Prototype

Since I'm a habitual experimenter, I didn't follow my own advice to stop there, and noticed several limitations, or areas that could be improved upon.  For example:

* The `expected` column was limited to testing equality.
* On the first failure, the test stops immediately.  Why not capture all failures?
* When it fails, it's not obvious which "row" failed.
* Logging the test-parameters in failure messages would be VERY useful.

I started out writing some sample test code that was plausible Scala syntax, and close to what I imagine a final result might look like.  Once I had an example of an idealized test-API, I went about trying to actually implement the result.  Here's what I came up with:

```scala
  test("What is this black magic?") {
    Verify[(Float,Float,Float),Float]{
      case (a,b,c) => new SomeClass(a).someFunction(b,c)
    }.expect(
      (0f,  0f, 0f) isFloat 0f,
      (0f,  5f, 5f) isFloat 10f,
      (10f, 1f, 2f) isFloat 13f,
      (10f, 5f, 5f) isFloat 20f,
      (-10f,5f, 5f) isFloat 0f,
      // Intentional failures...
      (0f,  0f, 0f) isFloat 100f,
      (0f,  5f, 5f) isFloat 100f,
    )
  }
```
Example failures:
```text
0.0 did not equal 100.0
  Params   : (0.0,0.0,0.0)
  Expected : 100.0
  Actual   : 0.0
       
10.0 did not equal 100.0
  Params   : (0.0,5.0,5.0)
  Expected : 100.0
  Actual   : 10.0
```
The code is much more concise when testing only one parameter at a time:

```scala
  def functionToTest(a: Int): Int = a + 10
  test("A simpler example - before") {
    assert(functionToTest(0)  == 10f)
    assert(functionToTest(5)  == 15f)
    assert(functionToTest(8)  == 18f)
    assert(functionToTest(-5) == 5f)
    assert(functionToTest(-8) == 2f)
  }

  test("A simpler example - after") {
    Verify[Int,Int]{
      functionToTest
    }.expect(
      //Input | Result
      0 is 10,
      5 is 15,
      8 is 18,
      -5 is 5,
      -8 is 2,
    )
  }
```

This prototype isn't quite ready to be published and widely used yet, but I thought it would be worth sharing in it's current state.  The code used to support the above style is included below.  I'll probably continue to iterate on this prototype, so we'll see where it leads.

Tests don't have to be ugly, verbose, and unmaintainable.  Invest the time and effort necessary to make clean maintainable tests, and the test will work for you.

```scala
// (WARNING: This is super-early prototype code)

import java.io.{PrintWriter, StringWriter}
import org.scalactic.source
import org.scalatest.exceptions.TestFailedException

class Verify[PARAMS,RESULT](function: PARAMS => RESULT) {
  def expect(conditions: Condition[PARAMS,RESULT]*)(implicit pos: source.Position): Verify[PARAMS, RESULT] = {
    val results = conditions.flatMap{case cond => {
      try {
        val result = function.apply(cond.params)
        Some(VerifyResult(cond.params, cond.matcher, Some(result)))
      } catch {
        case e:Throwable => Some(VerifyResult(cond.params, cond.matcher, None, Some(e)))
      }
    }}

    val errors = results.filter(_.failed)
    if (errors.nonEmpty) {
      val msg = errors.map(_.format()).mkString("")
      throw new TestFailedException(
        messageFun = _ => Some(msg),
        cause = None,
        pos = pos,
        payload = None
      )
    }
    this
  }
}

object Verify{
  def apply[PARAMS,RESULT](function: PARAMS => RESULT): Verify[PARAMS,RESULT] =
    new Verify[PARAMS,RESULT](function)
}

trait Matcher[T] {
  def expected: T
  def verify(actual: T): Boolean
  def failureMessage(actual: T): String
}

class Is[T](expectEquals: T) extends Matcher[T]{
  override def expected: T = expectEquals
  override def verify(actual: T): Boolean = {
    actual == expected
  }
  override def failureMessage(actual: T): String = {
    s"$actual did not equal $expected"
  }
}

object Is{
  def apply[T](expected: T) = new Is[T](expected)
  implicit class ExtendedIs[PARAMS](params: PARAMS) {
    def is[RESULT](expected: RESULT): Condition[PARAMS,RESULT] = {
      Condition(params, Is(expected))
    }
  }
}

class IsFloat(tolerance: Float)(expectEquals: Float) extends Matcher[Float]{
  override def expected: Float = expectEquals
  override def verify(actual: Float): Boolean = {
    (actual == expected) ||
      ((actual <= expected + tolerance) && (actual >= expected - tolerance))
  }
  override def failureMessage(actual: Float): String = {
    s"$actual did not equal $expected"
  }
}

object IsFloat{
  def apply(tolerance: Float)(expectEquals: Float): IsFloat = new IsFloat(tolerance)(expectEquals)
  implicit val ft: FloatTolerance = FloatTolerance(0.000001f)
  implicit class ExtendedIsFloat[PARAMS](params: PARAMS) {
    def isFloat(expected: Float)(implicit tolerance: FloatTolerance): Condition[PARAMS,Float] = {
      Condition(params, IsFloat(tolerance.value)(expected))
    }
  }
}

case class FloatTolerance(value: Float)

/** The result of verifying a condition. */
case class VerifyResult[PARAMS,RESULT](
  params: PARAMS,
  matcher: Matcher[RESULT],
  actual: Option[RESULT] = None,
  exception: Option[Throwable] = None,
) {
  lazy val passed: Boolean = actual.exists(matcher.verify)
  lazy val failed: Boolean = !passed
  private lazy val failMsg = actual.map(matcher.failureMessage).getOrElse(exception.map(_.getMessage).getOrElse("Unknown Result"))
  def format(): String = {
    if (!passed) formatFailure else ""
  }
  private lazy val formatFailure: String = {
    val actualMsg = actual.map("  Actual   : " + _.toString).getOrElse("")

    val exceptionMsg = exception.map(e => {
      val sw = new StringWriter
      sw.append("  Exception: ")
      e.printStackTrace(new PrintWriter(sw))
      sw.toString
    }).getOrElse("")

    s"""
       |$failMsg
       |  Params   : $params
       |  Expected : ${matcher.expected}
       |$actualMsg$exceptionMsg
       """.stripMargin
  }
}

/** Represents an input and expected result, when applied to some function being tested.*/
case class Condition[PARAMS,RESULT](
  params:PARAMS,
  matcher: Matcher[RESULT],
  clueMessage: Option[String] = None
) {
  def clue(clueMessage: String): Condition[PARAMS,RESULT] = copy(clueMessage = Some(clueMessage))
}
```