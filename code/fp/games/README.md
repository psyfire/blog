# Functional Programming Purity in Games

## Challenges
Pure-FP (Pure Functional Programming) in video-games provides an unique set of challenges:

* Predictable FP purity vs unpredictable game world
* Immutability vs mutable state everywhere
* Libraries are not designed around FP or purity
* Entities often need to know (have reference to) the world around it (global state)

Despite these challenges, the pursuit of purity has (or can have) many benefits:

* Pure Functions are reliable and testable.
* Separation of Mutability vs Immutability
* Reduced memory-usage
* Reduced side-effect spaghetti
* Thread safety

## Getting your Hands Dirty

Before we start writing our master-piece, lets get our hands dirty! VERY dirty!  Create a small-prototype, and throw out
all ideas of correct software-patterns, and everything you think you know about writing good software.  Learn the tools,
make something that actually works, and before adding any features hack away until it feels fun to play in it's simplest
form.

Once our prototype is in place, then we can start experimenting with FP-Purity.  Always remember FP-Purity is a tool that
helps us with the pursuit of writing cleaner software.  FP-Purity is not a binary-pursuit, where you've failed with any
impurity, but rather it's a spectrum where code can be improved and iterated on becoming more pure over time.  Even as you
progress towards FP-purity, you'll often find you'll have to get your hands very dirty again for a while in order to move
your game forward.

## Tricks

My hope is some of these tricks help you introduce greater FP-purity into your games.

* **Articles Coming Soon**

## Disclaimers

* The examples should be adaptable to a variety of languages and frameworks, but will mostly be focusing on Scala and LibGDX.
* FP-Purity in games is difficult and relatively unexplored territory.  It's very likely many of the ideas here will be replaced or improved upon over time.
* I intentionally called these `tricks` and not `patterns`.  Apply these tricks when it makes sense, don't treat them as the only correct way to write software.
* If you discover better ways, please share them!
