## Autodidacticism

Autodidacticism is the art of self-directed learning.  Autodidacticism shares many attributes with various other creative and innovative pursuits, as one explores a variety of unconventional paths to learning, knowledge, and expertise.

This section will focus on [https://en.wikipedia.org/wiki/Autodidacticism], along with several related subjects including side-projects, philosophy, creativity, and exploring the unknown.